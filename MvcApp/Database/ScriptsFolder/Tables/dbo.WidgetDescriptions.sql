CREATE TABLE [dbo].[WidgetDescriptions]
(
[WidgetID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[WidgetName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Active] [bit] NULL
) ON [PRIMARY]
GO
